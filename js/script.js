"use strict"

const load = document.querySelector("#load");
load.addEventListener("click", promise);
const URL = "https://swapi.dev/api/films/";

async function promise(){
    const response = await fetch(`${URL}`);
    const data = await response.json();
    const movies = document.getElementById('movies');
    movies.style.listStyle = "none";

   data.results
     .sort((a, b) => a.episode_id > b.episode_id)
     .map(movie => {

         // Movies
         const li = document.createElement("li");
         const ol = document.createElement("ol");
         const title = document.createElement("h2");
         const paragraph = document.createElement("p");
         const {episode_id, title: name, opening_crawl} = movie;
         title.textContent = `Episode ${episode_id} ${name}`;
         paragraph.textContent = opening_crawl;
         li.append(title);
         li.append(paragraph);
         li.append(ol);
         movies.append(li);

        // Characters
         movie.characters.forEach( async function (character){
             const li = document.createElement("li");
             const resolve = await fetch(character);
             const data = await resolve.json();
             li.textContent = data.name;
             ol.append(li);
         })
     })
     .join("")
};



